﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        SqlConnection connection;
        DataSet dataSt;
        string ID;
        private void selectData()
        {
            string sql = "SELECT * FROM customer";
            dataSt = new DataSet();
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    adapter.Fill(dataSt, "customer");
                }
            }
        }
            public Form1(string id)
        {
            ID = id;
            InitializeComponent();
        }
        private void updateData()
        {
            label1.Text = ID;
            string sql = "SELECT * FROM customer WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("id", ID);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                label1.Text = reader["name"].ToString();
                label2.Text = reader["cash"].ToString();
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            connection.Open();
            selectData();
            updateData();
            panel1.Controls.Clear();
            owner owner = new owner(ID, connection);
            panel1.Controls.Add(owner);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            shop shop = new shop(ID, connection);
            shop.OnPurchase += (s, args) => updateData();
            panel1.Controls.Add(shop);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            topup usrctrl = new topup(ID, connection);
            usrctrl.OnTopup += (s, args) => updateData();
            panel1.Controls.Add(usrctrl);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            owner owner = new owner(ID, connection);
            panel1.Controls.Add(owner);
        }
    }
}
